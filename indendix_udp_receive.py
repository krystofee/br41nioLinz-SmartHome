import logging
import socket
from collections import deque
from threading import Thread

from logging import getLogger

import time

from PyQt5.Qt import QApplication
from PyQt5.Qt import QMainWindow
from PyQt5.Qt import QLabel, QFont, QPixmap
from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSignal, QObject, QTimer

from spotify_prototype.spotify import SpotifyPlayer
from blinds_lights_controllers import LightsBlindsController

LIGHTS_BLINDS_PORT = 'COM8'


class IntendixReceiver:
    INTENDIX_OBJECT_START = b'PY_'
    INTENDIX_OBJECT_END = b'_E'

    WORKING_MODE = b'working_mode'
    LEISURE_MODE = b'leisure_mode'
    SLEEP_MODE = b'sleep_mode'
    PARTY_MODE = b'party_mode'
    ROMANTIC_MODE = b'romantic_mode'
    TURN_OFF = b'turn_off'  # control the BCI itself

    INTENDIX_OBJECT_MAX_SIZE = 64*1024*1024
    DECISION_REPEATS = 2

    def __init__(self, ip="0.0.0.0", port=10000):
        self.running = False
        self.ip = ip
        self.port = port
        self.message_map = {self.WORKING_MODE: self.set_working_mode,
                            self.LEISURE_MODE: self.set_leisure_mode,
                            self.SLEEP_MODE: self.set_sleep_mode,
                            self.PARTY_MODE: self.set_party_mode,
                            self.ROMANTIC_MODE: self.set_romantic_mode,
                            self.TURN_OFF: self.set_turn_off_mode,
        }
        self.logger = getLogger("DecisionReceiver")
        self.logger.setLevel(logging.INFO)

    def set_working_mode(self):
        print('Setting working mode')

    def set_sleep_mode(self):
        print('Setting sleep mode')

    def set_leisure_mode(self):
        print('Setting leisure mode')

    def set_party_mode(self):
        print('Setting party mode')

    def set_romantic_mode(self):
        print('Setting romantic mode')

    def set_turn_off_mode(self):
        print('Turn off')

    def run(self):
        if self.running is False:
            self.running = True
            self._command_thread = Thread(target=self._receiver_thread)
            self._command_thread.daemon = True
            self._speller_decision_deque = deque(maxlen=self.DECISION_REPEATS)
            self._command_thread.start()

    def stop(self):
        self.running = False

    def _receiver_thread(self):
        while self.running:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.sock.bind((self.ip, self.port))

            # you should open ports or sockets for the mode chenges here.
            try:
                data, addr = self.sock.recvfrom(self.INTENDIX_OBJECT_MAX_SIZE)
                item_half = data.split(self.INTENDIX_OBJECT_START, maxsplit=1)[1]
                item = item_half.split(self.INTENDIX_OBJECT_END, maxsplit=1)[0]
                self._speller_decision_deque.append(item)
                if len(self._speller_decision_deque) == self.DECISION_REPEATS:
                    if len(set(self._speller_decision_deque)) == 1:
                        self._speller_decision_deque.clear()
                        try:
                            self.message_map[item]()
                        except KeyError:
                            self.logger.warning("Unknown command received")
            except IndexError:
                self.logger.warning("Incompatable item name received")


def wait_until(func, check_every=1):
    while True:
        if func():
            break
        time.sleep(check_every)


class IntendixReceiverQt(IntendixReceiver):
    def __init__(self):
        super().__init__()
        self.spotify_player = SpotifyPlayer()

    def connectBlinds(self):
        self.lights_blinds_controller = LightsBlindsController(port=LIGHTS_BLINDS_PORT)

    def connectSignal(self, command_signal):
        self.command_signal = command_signal

    def set_working_mode(self):
        super().set_working_mode()
        self.command_signal.emit('Setting working mode')
        self.spotify_player.play(SpotifyPlayer.CATEGORY_WORK)
        self.lights_blinds_controller.set_blinds_level(0)
        self.lights_blinds_controller.set_lights(1.0, 1.0, 1.0)

    def set_sleep_mode(self):
        super().set_sleep_mode()
        self.command_signal.emit('Setting sleep mode')
        self.spotify_player.play(SpotifyPlayer.CATEGORY_SLEEP)
        self.lights_blinds_controller.set_blinds_level(0.8)
        self.lights_blinds_controller.set_lights(0.0, 0.0, 0.0)

    def set_turn_off_mode(self):
        super().set_sleep_mode()
        self.command_signal.emit('Turn off')
        self.spotify_player.pause()
        self.lights_blinds_controller.set_blinds_level(0.8)
        self.lights_blinds_controller.set_lights(0.0, 0.0, 0.0)

    def set_leisure_mode(self):
        super().set_leisure_mode()
        self.command_signal.emit('Setting leisure mode')
        self.spotify_player.play(SpotifyPlayer.CATEGORY_LEISURE)
        self.lights_blinds_controller.set_blinds_level(0.25)
        self.lights_blinds_controller.set_lights(0.7,0.7,0.5)

    def set_party_mode(self):
        super().set_party_mode()
        self.command_signal.emit('Setting party mode')
        self.spotify_player.play(SpotifyPlayer.CATEGORY_PARTY)
        self.lights_blinds_controller.set_blinds_level(0.5)
        self.lights_blinds_controller.set_party_mode()

    def set_romantic_mode(self):
        super().set_romantic_mode()
        self.command_signal.emit('Setting romantic mode')
        self.spotify_player.play(SpotifyPlayer.CATEGORY_ROMANTIC)
        self.lights_blinds_controller.set_blinds_level(0.4)
        self.lights_blinds_controller.set_lights(0.7,0.2,0.01)


class IndendixApp(QObject):
    command_signal = pyqtSignal(str)
    set_song_name_signal = pyqtSignal(str)
    def __init__(self):
        super().__init__()
        self.app = QApplication([])
        screen_resolution = self.app.desktop().screenGeometry()
        self.x_res, self.y_res = screen_resolution.width(), screen_resolution.height()
        self.main_panel = QMainWindow()
        self.main_panel.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.main_panel.move(0, 0)
        self.main_panel.setStyleSheet("background-color:black;")
        self.main_panel.resize(self.x_res, self.y_res/10*2.38)  # adapted to maximized speller window
        self.intendix_receiver = IntendixReceiverQt()
        self.intendix_receiver.connectSignal(self.command_signal)
        self.intendix_receiver.connectBlinds()

        self.main_text = QLabel(self.main_panel)
        self.main_text_font = QFont("Arial", 60, QFont.Bold)
        self.main_text.setText("Awaiting commands")
        self.main_text.setFont(self.main_text_font)
        self.main_text.setStyleSheet('color: white')
        self.main_text.move(1 / 50 * self.x_res, 0)
        self.main_text.resize(self.x_res, self.y_res*1.4/10)

        self.music_icon = QLabel(self.main_panel)
        pixmap = QPixmap('res/logo-spotify.png')
        pixmap = pixmap.scaledToWidth(48)
        pixmap = pixmap.scaledToHeight(48)
        self.music_icon.setPixmap(pixmap)
        self.music_icon.move(1 / 50 * self.x_res, 1.6 / 10 * self.y_res)
        self.music_icon.resize(48, 48)
        self.music_icon.hide()

        self.now_playing_text = QLabel(self.main_panel)
        self.now_playing_text_font = QFont("Arial", 23, QFont.Bold)
        self.now_playing_text.setFont(self.now_playing_text_font)
        self.now_playing_text.setStyleSheet('color: white')
        self.now_playing_text.move(3.5 / 50 * self.x_res, 1.65 / 10 * self.y_res)
        self.now_playing_text.resize(self.x_res, 40)

        self.command_signal.connect(self.set_command_text)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.check_spotify_timer_event)
        self.timer.setInterval(5000)
        self.timer.start()

    def check_spotify_timer_event(self):
        self.set_played_song_text(self.get_spotify_name())

    def get_spotify_name(self):
        return self.intendix_receiver.spotify_player.currently_playing()

    def set_played_song_text(self, song_name):
        if song_name:
            self.music_icon.show()
            self.now_playing_text.setText(song_name)
        else:
            self.music_icon.hide()
            self.now_playing_text.setText('')

    def set_command_text(self, text):
        self.main_text.setText(text)

    def run(self):
        self.main_panel.show()
        self.intendix_receiver.run()
        self.app.exec()

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)

    r = IndendixApp()
    r.run()
