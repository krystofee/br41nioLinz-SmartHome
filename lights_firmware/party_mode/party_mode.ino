#include <Adafruit_NeoPixel.h>

#ifdef __AVR__
#include <avr/power.h>
#include <Servo.h> 
Servo blind_servo;
#endif

int readChar = 0;

int data[4];

#define PIN 5

// Parameter 1 = number of pixels in strip

// Parameter 2 = Arduino pin number (most are valid)

// Parameter 3 = pixel type flags, add together as needed:

//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)

//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)

//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)

//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)

//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)

Adafruit_NeoPixel strip = Adafruit_NeoPixel(10, PIN, NEO_GRB + NEO_KHZ800);

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across

// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input

// and minimize distance between Arduino and first pixel.  Avoid connecting

// on a live circuit...if you must, connect GND first.

bool party_mode = false;
int blink_type = 0;
unsigned long time_ms;
unsigned long time_s;

void setup() {

  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket

  #if defined (__AVR_ATtiny85__)

    if (F_CPU == 16000000) clock_prescale_set(clock_div_1);

  #endif

  // End of trinket special code

  Serial.begin(9600);


  strip.begin();

  strip.show(); // Initialize all pixels to 'off'

  int current_blind_position = 255;
  blind_servo.attach(9);
  blind_servo.write(0);

}

void loop() {
  delay(100);
  protoxol_set();

  if (party_mode)
  {
      time_ms = millis();
      time_s = time_ms / 100;
      if (time_s % 3 != 0 )
      {
        for(int i=0; i< strip.numPixels(); i++) {    
          strip.setPixelColor(i, 0, 0 ,0);
        }
        strip.show();
        
      }
  
      else
         {
          blink_type++;
          if(blink_type>2)
                {
                  blink_type=0;
                }      
          for(int i=blink_type; i< strip.numPixels(); i=i+3) {
                time_ms = millis()/20;
                strip.setPixelColor(i, (time_ms + i * 30)%255, (time_ms+ i * 5)%255 , (time_ms+ i * 75)%255);
             }
          strip.show();
        
      
          }
    
  }


}




void protoxol_set() {

  if(Serial.available()>0)
  {
    delay(100);
  readChar = Serial.read();


  switch(readChar-48) {

    case 1:

      for(int i = 0; i < 3; i++) {

        data[i] = Serial.read();
        party_mode = false;

      }
        for(uint16_t i=0; i<strip.numPixels(); i++) {
          strip.setPixelColor(i, data[0], data[1], data[2]);
        }
        strip.show();

      break;

    case 2:

      data[3] = (int)Serial.read();
      blind_servo.write(data[3]*90/255);

      break;

    case 3:
       party_mode = true;
    default: break;

  }
  }

}
