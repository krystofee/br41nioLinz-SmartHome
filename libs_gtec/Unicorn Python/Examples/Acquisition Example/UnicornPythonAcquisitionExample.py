import UnicornPy as Unicorn

def main():
    # Specifications for the data acquisition.
    #-------------------------------------------------------------------------------------
    TestsignaleEnabled = True;
    FrameLength = 1;
    AcquisitionDurationInSeconds = 10;
    DataFile = "data.bin";

    print("Unicorn Acquisition Example")
    print("---------------------------")
    print()

    try:
        # Get available devices.
        #-------------------------------------------------------------------------------------

        # Get available device serials.
        deviceList = Unicorn.GetAvailableDevices(True)

        if len(deviceList) <= 0 or deviceList is None:
            raise Exception("No device available.Please pair with a Unicorn first.")

        # Print available device serials.
        print("Available devices:")
        i = 0
        for device in deviceList:
            print("#%i %s" % (i,device))
            i+=1

        # Request device selection.
        print()
        deviceID = int(input("Select device by ID #"))
        if deviceID < 0 or deviceID > len(deviceList):
            raise IndexError('The selected device ID is not valid.')

        # Open selected device.
        #-------------------------------------------------------------------------------------
        print()
        print("Trying to connect to '%s'." %deviceList[deviceID])
        handle = Unicorn.OpenDevice(deviceList[deviceID])
        print("Connected to '%s'." %deviceList[deviceID])
        print("Device Handle: %i" %handle)
        print()

        # Create a file to store data.
        file = open(DataFile, "wb")

        # Initialize acquisition members.
        #-------------------------------------------------------------------------------------
        numberOfAcquiredChannels= Unicorn.GetNumberOfAcquiredChannels(handle)
        configuration = Unicorn.GetConfiguration(handle)
        samplingRate = configuration.SamplingRate;

        # Print acquisition configuration
        print("Acquisition Configuration:");
        print("Sampling Rate: %i Hz" %samplingRate);
        print("Frame Length: %i" %FrameLength);
        print("Number Of Acquired Channels: %i" %numberOfAcquiredChannels);
        print("Data Acquisition Length: %i s" %AcquisitionDurationInSeconds);
        print();

        # Allocate memory for the acquisition buffer.
        receiveBufferBufferLength = FrameLength * numberOfAcquiredChannels * 4
        receiveBuffer = bytearray(receiveBufferBufferLength)
    
        try:
            # Start data acquisition.
            #-------------------------------------------------------------------------------------
            Unicorn.StartAcquisition(handle, TestsignaleEnabled)
            print("Data acquisition started.")

            # Calculate number of get data calls.
            numberOfGetDataCalls = int(AcquisitionDurationInSeconds * samplingRate / FrameLength);
        
            # Limit console update rate to max. 25Hz or slower to prevent acquisition timing issues.                   
            consoleUpdateRate = int((samplingRate / FrameLength) / 25.0);
            if consoleUpdateRate == 0:
                consoleUpdateRate = 1

            # Acquisition loop.
            #-------------------------------------------------------------------------------------
            for i in range (0,numberOfGetDataCalls):
                # Receives the configured number of samples from the Unicorn device and writes it to the acquisition buffer.
                Unicorn.GetData(handle,FrameLength,receiveBuffer,receiveBufferBufferLength)
            
                # Write data to file.
                file.write(receiveBuffer)

                # Update console to indicate that the data acquisition is running.
                if i % consoleUpdateRate == 0:
                    print('.',end='',flush=True)

            # Stop data acquisition.
            #-------------------------------------------------------------------------------------
            Unicorn.StopAcquisition(handle);
            print()
            print("Data acquisition stopped.");

        except Unicorn.DeviceException as e:
            print("An error occured. Error Code: %s" %e)
        except Exception as e:
            print("An unknown error occured. %s" %e)
        finally:
            # release receive allocated memory of receive buffer
            del receiveBuffer

            # Close device.
            #-------------------------------------------------------------------------------------
            handle = Unicorn.CloseDevice(handle)
            print("Disconnected from Unicorn")

    except Unicorn.DeviceException as e:
        PrintExceptionMessage(e)
    except Exception as e:
        print("An unknown error occured. %s" %e)

    input("\n\nPress ENTER key to exit")

#This Method writes an error message to the console if a DeviceException was caught.
def PrintExceptionMessage( exception ):
    errorMessages = {
    Unicorn.ErrorInvalidParameter: "One of the specified parameters does not contain a valid value.",
    Unicorn.ErrorBluetoothInitFailed: "The initialization of the Bluetooth adapter failed.",
    Unicorn.ErrorBluetoothSocketFailed: "The operation could not be performed because the Bluetooth socket failed.",
    Unicorn.ErrorOpenDeviceFailed: "The device could not be opened.",
    Unicorn.ErrorInvalidConfiguration: "The configuration is invalid.",
    Unicorn.ErrorBufferOverflow: "The acquisition buffer is full.",
    Unicorn.ErrorBufferUnderflow: "The acquisition buffer is empty.",
    Unicorn.ErrorOperationNotAllowed: "The operation is not allowed.",
    Unicorn.ErrorNotImplemented: "The called function is not implemented yet.",
    Unicorn.ErrorInvalidHandle: "The specified connection handle is invalid.",
    Unicorn.ErrorUnknownError: "An unspecified error occurred.",
    }  

    errorCode = exception.args[0]
    errorMessage = errorMessages.get(errorCode, "Unknown An unknown error occured.")
    print("An error occured. Error Code: {0} - {1}".format(errorCode, errorMessage))

#execute main
main()