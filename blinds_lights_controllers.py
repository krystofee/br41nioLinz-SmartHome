from serial import Serial


class AbstractSerialController:
    def __init__(self, port='COM5'):
        self.port = Serial(port)


class LightsControllerMixin:
    LIGHTS_CONTROL_TYPE = 1 + 48
    LIGHTS_PARTY_TYPE = 3 + 48
    def set_lights(self, r, g, b):
        '''rgb light levels from 1 to 0'''
        self.port.write(bytearray([self.LIGHTS_CONTROL_TYPE]))
        colors = [r, g, b]
        for i in range(len(colors)):
            colors[i] = int(colors[i] * 255)
            if colors[i] > 255:
                colors[i] = 255
            elif colors[i] < 0:
                colors[i] = 0
        self.port.write(bytearray(colors))
        self.port.flush()

    def set_party_mode(self):
        self.port.write(bytearray([self.LIGHTS_PARTY_TYPE]))
        self.port.flush()


class LightsBlindsController(LightsControllerMixin, AbstractSerialController):
    BLINDS_CONTROL_TYPE = 2 + 48
    def set_blinds_level(self, x):
        '''x = 1 fully closed, 0 - fully open'''
        self.port.write(bytearray([self.BLINDS_CONTROL_TYPE]))
        blinds_level = int(x * 255)
        if blinds_level > 255:
            blinds_level = 255
        elif blinds_level < 0:
            blinds_level = 0
        self.port.write(bytearray([blinds_level]))
        self.port.flush()
