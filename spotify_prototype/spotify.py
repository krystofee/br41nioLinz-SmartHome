import requests
import json
import random

import spotipy.util as util

from .config import REDIRECT_URI, CLIENT_ID, CLIENT_SECRET, USERNAME, SCOPE


class SpotifyPlayer:
    debug = False
    token = None

    API_BASE = 'https://api.spotify.com/v1'

    CATEGORY_SLEEP = 'sleep'
    CATEGORY_WORK = 'focus'
    CATEGORY_LEISURE = 'chill'
    CATEGORY_ROMANTIC = 'romance'
    CATEGORY_PARTY = 'party'

    PREDEFINED_PLAYLISTS = {
        CATEGORY_SLEEP: 'spotify:user:spotify:playlist:37i9dQZF1DWYcDQ1hSjOpY',
        CATEGORY_WORK: 'spotify:user:11144916141:playlist:0oxdoQsP23yswNXnHnmavY',
        CATEGORY_LEISURE: 'spotify:user:11144916141:playlist:0W9NtJJCUeVH95V3NRXDcE',
        CATEGORY_ROMANTIC: 'spotify:user:11144916141:playlist:3FvrKRSQbWw7fiBxUDV6vT',
        CATEGORY_PARTY: 'spotify:user:11144916141:playlist:2h1WOOZfmHkyE3obsQS0UO',
    }

    def __init__(self, predefined_playlists=False, debug=False):
        self.predefined_playlists = predefined_playlists
        self.debug = debug
        self.token = util.prompt_for_user_token(
            USERNAME,
            SCOPE,
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            redirect_uri=REDIRECT_URI,
        )
        self.auth_headers = {
            'Authorization': f'Bearer {self.token}'
        }
        # turn shuffle on for the player
        requests.put(f'{self.API_BASE}/me/player/shuffle', headers=self.auth_headers)
        if self.debug:
            print('Got auth token below:')
            print(self.token)

    @property
    def player_info(self):
        if not hasattr(self, '_cached_player_info'):
            response = requests.get(f'{self.API_BASE}/me/player', headers=self.auth_headers)
            self._cached_player_info = json.loads(response.content.decode())
        return self._cached_player_info

    @property
    def player_id(self):
        return self.player_info['device']['id']

    def playlists(self, category):
        response = requests.get(f'{self.API_BASE}/browse/categories/{category}/playlists', headers=self.auth_headers)
        return json.loads(response.content.decode())['playlists']

    def random_playlist_for_category(self, category):
        playlists = self.playlists(category)['items']
        result = random.choice(playlists)
        if self.debug:
            print(f'Random playlist: {result["name"]}')
        return result

    def play(self, category=None):
        headers = self.auth_headers
        body = {}
        if category:
            if not self.predefined_playlists:
                context_uri = f'spotify:user:spotify:playlist:{self.random_playlist_for_category(category)["id"]}'
            else:
                context_uri = self.PREDEFINED_PLAYLISTS[category]
            body['context_uri'] = context_uri
        if self.debug:
            print(f'Starting playing playlist: {category}')
        response = requests.put(
            f'{self.API_BASE}/me/player/play',
            data=json.dumps(body),
            headers=headers
        )
        return response

    def pause(self):
        if self.debug:
            print('Pausing player.')
        return requests.put(f'{self.API_BASE}/me/player/pause', headers=self.auth_headers)

    def currently_playing(self):
        response = requests.get(
            f'{self.API_BASE}/me/player',
            headers=self.auth_headers
        )
        if not response.content:
            return None
        content = json.loads(response.content.decode())
        artists = ', '.join(map(
            lambda x: x['name'],
            content['item']['album']['artists']
        ))
        song_name = content['item']['name']
        return f'{artists} - {song_name}'


# NOTE some testing data
#import time
#sp = SpotifyPlayer(debug=True)
#print(sp.currently_playing())
#sp.play(SpotifyPlayer.CATEGORY_LEISURE)
#time.sleep(5)
#sp.play(SpotifyPlayer.CATEGORY_PARTY)
#time.sleep(5)
#sp.play(SpotifyPlayer.CATEGORY_ROMANTIC)
#time.sleep(5)
#sp.play(SpotifyPlayer.CATEGORY_SLEEP)
#time.sleep(5)
#sp.play(SpotifyPlayer.CATEGORY_WORK)
#time.sleep(5)
#sp.pause()
